jQuery(document).ready(function($){
    $(document).on("click",".color-text .nbd-swatch-wrap label",function (e) {
        jQuery(this).attr("title");
        var textColor = jQuery(this).attr("title");
        jQuery(".color-text .nbd-field-header>label").text('color: '+ textColor); 
    });
    $(document).on('click','.input-number-increment',function (e) {
        var quality = $(this).closest('.quantity-custom').find('.input-number');
        var quality_val = quality.val();
        var new_quality = parseInt(quality_val) + 1;
        quality.val(new_quality)
    })
    $(document).on('click','.input-number-decrement',function (e) {
        var quality = $(this).closest('.quantity-custom').find('.input-number');
        var quality_val = quality.val();
        var new_quality = parseInt(quality_val) - 1;
        if(new_quality >= 0){
            quality.val(new_quality)
        }
    })
    var url         = $(".custom_button").attr("data-url")
    var id          = $(".single_add_to_cart_button").attr("data-product_id")
    var new_url     = url+id
    $(".custom_button").attr("href", new_url)
});
